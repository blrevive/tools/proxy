#pragma once
#include <memory>
#include <string>
#include <map>
#include <fstream>
#include <nlohmann/json.hpp>
#include <Proxy/Logger.h>
#include <Proxy/Utils.h>
#include <Proxy/Errors.h>
#include <sys/stat.h>
#include <Proxy/resource.h>
#include <Proxy/Resources.h>

using json = nlohmann::json;
namespace fs = std::filesystem;

namespace BLRevive
{
	struct ConfigProvider { ConfigProvider(json _json) {}; };
	struct ProxyConfig : ConfigProvider
	{
		struct TServer 
		{
			std::string Host;
			int Port;
		} Server;

		struct TLogging
		{
			int Level;
		} Logging;

		ProxyConfig(json _json) : ConfigProvider(_json)
		{
			Server.Host = _json["Server"]["Host"];
			if (_json["Server"]["Port"].is_string()) {
				std::string sPortCalc(_json["Server"]["Port"]);
				int sCalcNum = std::stoi(sPortCalc.substr(1));
				std::string sCalcOp = sPortCalc.substr(0, 1);

				int BlrRvPort = Utils::URL::Param::Int("Port", 7777);
				if (sCalcOp == "+")
					Server.Port = BlrRvPort + sCalcNum;
				else if (sCalcOp == "-")
					Server.Port = BlrRvPort - sCalcNum;
				else
					Server.Port = std::stoi(sPortCalc);
			}
			else {
				Server.Port = _json["Server"]["Port"];
			}
		}
	};

	class Config : public Utils::ProcessSingleton<Config>
	{
	public:
		static std::shared_ptr<Config> From(std::string Filename)
		{
			try {
				std::string FilePath = Utils::FS::BlreviveConfigPath() + "\\" + Filename;
				json _json;
				std::ifstream file(FilePath);
				file >> _json;
				file.close();
				return std::make_shared<Config>(_json);
			}
			catch (json::exception e) {
				throw new Errors::failure(("Config: failed to initialize " + Filename + " because of " + e.what()).c_str(), Errors::ExitReason::CONFIG_INIT_FAIL);
			}
			catch (std::exception e) {
				throw new Errors::failure(("Config: failed to initialize " + Filename + " because of " + e.what()).c_str(), Errors::ExitReason::CONFIG_INIT_FAIL);
			}
		}

#if IS_PROXY_PROJECT
		static std::shared_ptr<Config> Create(std::string Filename)
		{
			// check if default config exists
			std::string fullpath(Utils::FS::BlreviveConfigPath() + "\\" + Filename);
			if (Filename == "default.json" && !std::ifstream(fullpath).good()) {
				SaveDefaultConfigFromResources();
			}
			_self = From(Filename);
			return _self;
		}

		/// <summary>
		/// Saves the default config from resources to disk.
		/// </summary>
		static void SaveDefaultConfigFromResources()
		{
			auto defaultConfig = Resources::Get<nlohmann::json>(RES_BLRV_DEF_CONFIG);
			std::ofstream file(Utils::FS::BlreviveConfigPath() + "\\default.json");
			file << defaultConfig.dump(4);
			file.close();
		}
#endif
		inline json& Get() { return _json; }

		inline json& Get(std::string key)
		{
			if (_json.contains(key))
				return _json[key];
			return (json&)"";
		}

		template<typename TConf>
		inline TConf& Get(std::string key)
		{
			try {
				if (_ConfigProviders.count(key) > 0)
					return *(TConf*)_ConfigProviders[key];

				if (_json.contains(key)) {
					auto conf = new TConf(_json[key]);
					_ConfigProviders[key] = conf;
					return *conf;
				}
			}
			catch (int e) {
				throw Errors::failure("Failed to parse config to class.");
			}
		}

		Config(json jConfig) { _json = jConfig; }
	protected:
		json _json;
		std::map<std::string, ConfigProvider*> _ConfigProviders = std::map<std::string, ConfigProvider*>();

	};
}