#pragma once
#include <memory>
#include <Proxy/Logger.h>
#include <Proxy/Events.h>
#include <Proxy/Network.h>
#include <semver.hpp>
#include <string>

namespace BLRevive::Module
{
	/**
	 * Information about a module
	 */
	struct Info
	{
		std::string Name;
		semver::version Version;
		semver::version ProxyVersion;
		bool SupportsServer;
		bool SupportsClient;

		Info() : Name(""), Version(semver::version{""}), ProxyVersion(semver::version{ "" }), SupportsClient(false), SupportsServer(false) {}
		Info(nlohmann::json _j) {
			_j.at("Name").get_to(Name);
			_j.at("SupportsServer").get_to(SupportsServer);
			_j.at("SupportsClient").get_to(SupportsClient);

			std::string sVersion(_j["Version"]);
			std::string sProxyVersion(_j["ProxyVersion"]);
			Version = semver::version{ sVersion };
			ProxyVersion = semver::version(sProxyVersion);
		}
	};

	/**
	 *  Initialization data for modules 
	 */
	struct InitData
	{
		// pointer to event manager singleton
		std::shared_ptr<Events::Manager> EventManager;
		// pointer to logger singleton
		std::shared_ptr<Logger> Logger;
		// pointer to server singleton
		std::shared_ptr<Network::Server> Server;
	};

#if IS_PROXY_PROJECT
	/**
	 * Type of Module
	 */
	enum class Type
	{
		SERVER,
		CLIENT
	};

	/**
	* Manager class for module loading
	*/
	class Loader
	{
	public:
		// module initializer function signature
		typedef void(*Initializer)(InitData*);
		typedef Info& (*ModuleInfoGetter)();

		/**
		 * Get modules mapped by type from cofig
		 * 
		 * @return std::map<Type, std::vector<std::string>> modules in config
		 */
		static std::map<Type, std::vector<std::string>> GetFromConfig();

		/**
		 * Load all modules defined in config
		 */
		static void Load();

		/**
		 * Load a single module
		 * 
		 * @param  FileName	module filename
		 */
		static void Load(std::string FileName);
	protected:
		// current loaded modules
		inline static std::vector<std::tuple<Type, std::string, HINSTANCE>> LoadedModules = {};
	};
#endif
}