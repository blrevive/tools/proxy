
# Config

To get access to the current configuration inside your module you can use the `BLRevive::Config` class.

The best practice is to define your config as struct and use the json converter to get an instance of the config.

```c++
#include <vector>
#include <string>
#include <Proxy/Config.h>
#include <Proxy/Logger.h>

using namespace BLRevive;
using json = nlohmann::json;

// your module config as struct
struct MyModuleConfig
{
    // you can use any native type
    std::vector<int> SomeIntParams;

    // sub-structs for better naming
    struct SubStruct {
        std::string SomeString;
    } Sub;

    // the constructor retrieves the json and you need to assign your values
    MyModuleConfig(json j) {
        SomeIntParams = (std::vector<int>)j["SomeIntParams"];
        Sub.SomeString = (std::string)j["Sub"]["SomeString"];
    }
};

void DoSomethingWithConfig()
{
    auto config = Config::GetInstance();
    auto moduleConfig = config->Get<MyModuleConfig>("MyModule");

    for(int i : moduleConfig.SomeIntParams)
        LInfo("I: {}", i);

    LInfo("SomeString: {}", moduleConfig.Sub.SomeString);
}
```

# Logger

You can use our logger implementation by including `<Proxy/Logger.h>` which allows you to log into the same file as the proxy does `<BlrDir>\FoxGame\Logs\BLRevive-<timestamp>.log`.

## usage

```c++
#include <Proxy/Logger.h>

void SpamLog()
{
    // all log macros use the same calling convention
    //  which is equal to spdlog::log()
    LInfo("An info string using {} {}", "templated", "placeholders");
    LDebug("A debug message");
    LError("An error message for {} in {}", error.what(), "MyFile");
    LCritical("A critical message!");

    // instantly write all log messages and clear buffer
    LFlush;
}
```