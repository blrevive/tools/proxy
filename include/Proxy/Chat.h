#pragma once
#include <string>
#include <Proxy/Events.h>

namespace BLRevive
{
	class Chat
	{
	public:
		static void Init();
		static void AddCommand(std::string command, std::function<void(void* ui, std::string message)> callback);

	protected:
		inline static std::map<std::string, int> CommandIDs = std::map<std::string, int>();
		inline static std::map<int, std::function<void(void*, std::string)>> Callbacks = std::map<int, std::function<void(void*, std::string)>>();
	};
};