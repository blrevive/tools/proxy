#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>

#include <vector>
#include <map>
#include <functional>
#include <fstream>
#include <filesystem>
#include <nlohmann/json.hpp>

// disable warnings from sdk
#pragma warning(disable:4244)
#include <Proxy/Offsets.h>
#include <SdkHeaders.h>
#include <Proxy/Logger.h>
#include <Proxy/Network.h>
#include <Proxy/Modules.h>
#include <Proxy/Events.h>
#include <Proxy/Chat.h>

namespace fs = std::filesystem;
using namespace std;
using namespace BLRevive;


#pragma region Process Event Detour
// typedef for process event
typedef void(__thiscall* tProcessEvent)(class UObject*, class UFunction, void*);
// typedef for process event wrapper
typedef bool(*tProcessEventWrapper)(const class UObject* Object, const class UFunction* Function, const void* Params);

// pointer to current process event wrapper
tProcessEventWrapper pProcessEventWrapper;
// pointer to process event
tProcessEvent ProcessEvent = (tProcessEvent)pProcessEvent;

// pointers for process event
static DWORD dwProcessEventReturn = (DWORD)pProcessEventMidHookReturn;
static DWORD dwProcessEventSkip = (DWORD)pProcessEventMidHookEndReturn;
static DWORD dwPEReturn = (DWORD)(pProcessEventMidHookReturn + 0xA);

// current object of ProcessEvent
DWORD pObject = NULL;
// current function of ProcessEvent
DWORD pFunction = NULL;
// current params of ProcessEvent
DWORD pParams = NULL;

/**
* A mid-function hook handler for ProcessEvent.
* Gets the current objects and params and executes the wrapper.
*/
void __declspec(naked) hkProcessEvent()
{
	__asm
	{
		// read pointers from stack
		MOV			pFunction, EDI;
		MOV			pParams, EBX;
		MOV			pObject, ESI;

		// save all registers
		PUSHAD;
		PUSHFD;
	}

	// if the wrapper returns true, process event will be executed
	// if the wrapper returns false, process event will be skipped
	if (pProcessEventWrapper((UObject*)pObject, (UFunction*)pFunction, (void*)pParams))
	{
		__asm
		{
			// restore registers
			POPFD;
			POPAD;

			// execute overwritten instruction and jmp to next
			TEST		DWORD PTR[EDI + 0x84], 0x402;
			JMP[dwPEReturn];
		}
	}
	else
	{
		__asm
		{
			// restore registers
			POPFD;
			POPAD;

			// execute overwritten instruction and jmp to end
			TEST		DWORD PTR[EDI + 0x84], 0x402;
			JMP[dwProcessEventSkip];
		}
	}
}
#pragma endregion

#pragma region Process Event Wrapper
/**
 * Wrapper function for hooked ProcessEvent
 * 
 * @param  pObject 		called object
 * @param  pFunction 	called function
 * @param  pParams 		call parameters
 * @return bool 		true if PE should continue
 */
bool ProcessEventWrapper(UObject* pObject, UFunction* pFunction, void* pParams)
{
	// check if object and function is valid
	if (!pObject || !pFunction)
	{
		LError("ProcessEvent: Object or Function is null! (Object: {0:X} | Function: {1:X})", (DWORD)pObject, (DWORD)pFunction);
		LFlush;
		return false;
	}

	// check and update events
	Events::Manager::GetInstance()->Tick(pObject, pFunction, pParams);
	return true;
}
#pragma endregion

#pragma region Event Manager Implementations
/**
 * Process and sort events immedietly
 * 
 * @param  pObj 	called object
 * @param  pFunc 	called function
 * @param  pParams 	call parameters
 */
void Events::Manager::Tick(UObject* pObj, UFunction* pFunc, void* pParams)
{
	if (!pObj || !pFunc)
		return;
	// for some reason the only safe way to get UObject names while in ProcessEvent
	// is to call UObject->Name.GetName()
	std::string objName(pObj->Name.GetName());
	std::string funcName(pFunc->Name.GetName());

	// create info struct for current event
	auto info = Events::Info(pObj, pFunc, pParams);
	bool hasLazy = false;

	WaitForSingleObject(handlerMutex, INFINITE);
	for (auto it = _Handlers.begin(); it != _Handlers.end();) {
		auto handler = *it;
		bool matches = (handler.EventID.sObject == "*" || handler.EventID.sObject == objName) && (handler.EventID.sFunction == "*" || handler.EventID.sFunction == funcName);
		
		if (matches) {
			if (handler.Immediate) {
				handler.Callback(info);
				if (handler.SingleRun) {
					it = _Handlers.erase(it);
					continue;
				}
			}
			else {
				hasLazy = true;
			}
		}
		++it;
	}
	ReleaseMutex(handlerMutex);

	if (hasLazy) {
		LDebug("Add lazy handler event for {}->{}", objName, funcName); LFlush;

		void* buffer = pParams;
		// converse call parameters for queue
		if (pParams != NULL && pFunc->PropertySize > 0) {
			buffer = malloc(pFunc->PropertySize);
			if(buffer)
				std::memcpy(buffer, pParams, pFunc->PropertySize);
			info.Params = buffer;
		}
		else {
			info.Params = NULL;
		}

		// push event to queue
		_Events.push(info);
	}
}

/**
 * Process enqueued lazy events
 */
void Events::Manager::ProcessLazyEvents()
{
	WaitForSingleObject(handlerMutex, INFINITE);

	while (!_Events.empty()) {
		Events::Info Event = _Events.front();
		std::string objName(Event.Object->Name.GetName());
		std::string funcName(Event.Function->Name.GetName());
		for (auto it = _Handlers.begin(); it != _Handlers.end();) {
			auto handler = *it;
			bool matches = (handler.EventID.sObject == "*" || handler.EventID.sObject == objName) && (handler.EventID.sFunction == "*" || handler.EventID.sFunction == funcName);

			if (matches && !handler.Immediate) {
				handler.Callback(Event);
				if (handler.SingleRun) {
					it = _Handlers.erase(it);
					continue;
				}
			}
			++it;
		}

		_Events.pop();
	}
	ReleaseMutex(handlerMutex);
};

/**
 * Initialize EventManager.
 * Hooks ProcessEvent and starts event queue.
 * 
 * @return bool initialization succeed
 */
bool Events::Manager::Initialize()
{
	LDebug("Initializing EventManager");

	handlerMutex = CreateMutex(NULL, FALSE, NULL);

	// setup process event detour
	pProcessEventWrapper = (tProcessEventWrapper)ProcessEventWrapper;
	if (!Utils::Memory::WriteJMP((BYTE*)pProcessEventMidHookReturn, (DWORD)hkProcessEvent, 0xA))
	{
		LDebug("EventManager: Failed to setup ProcessEvent detour.");
		return false;
	}

	LDebug("EventManager is initialized");
	return true;
}
#pragma endregion

#pragma region Module Loader

typedef HMODULE (__stdcall *tLoadLibrary)(LPCSTR lpLibFileName);

void Module::Loader::Load(std::string ModuleName)
{
	static auto ProxyInfo = Module::Info(Resources::Get<nlohmann::json>(RES_BLRV_MODULE_INFO));
	std::string ModuleFile(Utils::FS::BlreviveModulePath() + ModuleName + ".dll");

	// load the dll	
	HINSTANCE hModule = LoadLibraryA(ModuleFile.c_str());
	if (!hModule) {
		LError("ModuleLoader::LoadModule: Failed to load module file {}  because of {}", ModuleFile, GetLastError()); LFlush;
		return;
	}

	try {
		Module::Info mInfo(Resources::Get<nlohmann::json>(100, "TEXT", ModuleName));

		// check if module version matches with current proxy version
		if (mInfo.ProxyVersion != ProxyInfo.Version) {
			LError("ModuleLoader::LoadModule: Module {} requires version {} but proxy is {}!", mInfo.Name, mInfo.ProxyVersion.to_string(), ProxyInfo.Version.to_string()); LFlush;
			return;
		}
	}
	catch (int e) {
		LError("Module info failed: {}", e);
		return;
	}


	// get initializer function
	auto initFunction = (Loader::Initializer)GetProcAddress(hModule, "InitializeModule");
	if (!initFunction) {
		LError("ModuleLoader::LoadModule: Module {} has no initializer!", ModuleFile); LFlush;
		return;
	}

	auto moduleThreadFunction = (void*)GetProcAddress(hModule, "ModuleThread");
	if (!moduleThreadFunction) {
		LError("ModuleLoader::LoadModule: Module {} has no module thread function!", ModuleFile); LFlush;
		return;
	}

	// initialize module
	Module::InitData startInfos {
		Events::Manager::GetInstance(),
		Logger::GetInstance(),
		Network::Server::GetInstance()
	};

	initFunction(&startInfos);

	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)moduleThreadFunction, NULL, NULL, NULL);

	LDebug("ModuleLoader::LoadModule Loaded {0}", ModuleFile); LFlush;
}

void Module::Loader::Load()
{
	auto modules = GetFromConfig();
	auto modsToLoad = Utils::IsServer() ? modules[Type::SERVER] : modules[Type::CLIENT];
	
	for (auto mod : modsToLoad)
		Load(mod);
}

std::map<Module::Type, std::vector<std::string>> Module::Loader::GetFromConfig()
{
	try {
		auto conf = Config::GetInstance()->Get("Proxy")["Modules"];
		std::vector<std::string> serverModules = conf["Server"];
		std::vector<std::string> clientModules = conf["Client"];

		std::map<Module::Type, std::vector<string>> mods{
			{Module::Type::SERVER, serverModules},
			{Module::Type::CLIENT, clientModules}
		};

		return mods;
	}
	catch (json::exception e) {
		LError("Failed to get modules from config: {}", e.what()); LFlush;
	}
	catch (int e) {
		LError("Failed to get modules from config! {}", e); LFlush;
	}

	return std::map<Module::Type, std::vector<std::string>>();
}

#pragma endregion

#pragma region Chat Interface

void Chat::Init()
{
	Events::Manager::GetInstance()->RegisterHandler({
		{"FoxChatUI", "ProcessSlashCommand"},
		[=](Events::Info eventInfo) {
			UFoxChatUI_eventProcessSlashCommand_Parms* parms = (UFoxChatUI_eventProcessSlashCommand_Parms*)eventInfo.Params;
			int comId = std::stoi(std::to_string(parms->ComIndex));

			auto clb = Callbacks[comId];
			clb(eventInfo.Object, parms->Message.ToChar());
		}, true, false
	});

	Chat::AddCommand("/test", [&](void* pUI, std::string message) {
		auto ui = (UFoxChatUI*)pUI;
		std::string reply = std::string("Test") + ": " + message;

		ui->eventTryUpdateLog(1, FString("BLRevive"), FString("test"), 0);
	});	
}

void Chat::AddCommand(std::string cmd, std::function<void(void*, std::string)> clb)
{
	static UFoxChatUI* chat = UObject::GetInstanceOf<UFoxChatUI>(true);

	TArray<FString> strs = chat->SlashCommands.Copy();
	strs.Add(FString(cmd.c_str()));

	chat->SlashCommands.Data = strs.Data;
	chat->SlashCommands.Count = CommandIDs[cmd] = strs.Count;

	Callbacks[strs.Count] = clb;
}

#pragma endregion