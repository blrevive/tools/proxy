A module system for Blacklight: Retribution.

When the proxy is attached to an instance of BLR it initializes static interfaces in order to load other modules and provide them with useful tools.

# installation

- use patcher provided with [Launcher]() **or**:
    - copy `Proxy.dll` to `<BlrDir>\Binaries\Win32\Proxy.dll`
    - add `Proxy.dll` entry to PE header of copy of `FoxGame-win32-Shipping.exe`
- copy modules to `<BlrDir>\Binaries\Win32\Modules\<Modulename>.dll`
- *(optional)* customize (see configuration below)

# configuration

Configuration files are located in `<BlrDir>\FoxGame\Config\BLRevive\<ConfigName>.json`. You can pass a `config=<ConfigName>` parameter to the start url of Blacklight like this: `BLR.exe server HeloDeck?config=<ConfigName>` to use a custom config. 

If the config parameter is given but no such file found, the app will exit. If no parameter is given it will load `default.json`. 

<details>
<summary><b>Example Configuration</b></summary>

The structure of the config is defined like this:
```json
{
    // configuration of the proxy itself
    "Proxy": {
        // read more about the embed server in features below
        "Server": {
            // enable the embed server
            "Enabled": true,
            // host of embed server
            "Host": "0.0.0.0",
            // port of embed server
            //  either string with (+-\d) (relates to port that BLR uses)
            //  or absolute integer
            "Port": "+1"
        },
        // load other modules
        "Modules": {
            // load these modules if running as server
            "Server": [
                // the Loadout Manager is a good example of  
                // how this proxy works in detail
                //  https://gitlab.com/blrevive/tools/loadout-manager
                "LoadoutManager"
            ],
            // load these modules if running as client
            "Client": [
                // you can load modules as server and client
                //  make sure the module supports both! 
                "LoadoutManager"
            ]
        },
        // configuration of modules using "<ConfigName>" as key
        "LoadoutManager": {
            // look into the module description 
            //  for its configuration
            ...
        }
    }
}
```

</details>

# Features

## Modules (`src/Modules.h`)

After a BLR instance with injected Proxy is started the `ModuleManager` will load modules which are provided in the config.

Modules configured in `Proxy\Modules\Server` are loaded only for server instances whereas `Proxy\Modules\Client` modules are loaded only for client instances. You can add the same module in both arrays *if the module works for both instances*.

## Embed Server (`src/Network.h`)

Because hijacking the UE3 network implementation (`USubSystem`) currently is not possible, the proxy starts a second http server which allows client-server connections and other data transfer.

> The [LoadoutManager]() is a good example of this, it uses the embed server to send player loadouts from client instances to server instances to allow custom loadouts.

### config

The embed server will listen for connections on (config)`Proxy\Server\Host`:`Proxy\Server\Port`.

`Proxy\Server\Port` should be either an absolute integer (like `7778`) or a string with add/substract instruction (like `+1`).
The calculation relates to the game port of BLR.

## Event API (`src/Events.h`)

The Event API is an interface for hooking `UObject::ProcessEvent` which handles almost all UE3 events. It can be used to listen for UE3 events within modules. 

**Read more at [Doc/Events](doc/Events.md)**

## Helpers/Utilities

### Config (`src/Config.h`)

The config provides access to the current json config and utilities to convert them to native structs.

**Read more at [Doc/Helpers#Config](doc/Helpers.md#Config)**

### Logger (`src/Logger.h`)

The logger provides easy, thread-safe logging macros in order to unify the log file to a single output.

**Read more at [Doc/Helpers#Logger](doc/Helpers.md#Logger)**

### BLR SDK (`lib/sdk/SdkHeaders.h`)

The [BLR SDK]() is a community developed SDK for Blacklight: Retribution. It provides allmost all native UE3 interfaces which can be used to retrieve and manipulate data of the game state.

**Read more at [BLR SDK](https://gitlab.com/blrevive/tools/sdk)**

# develop modules

The easiest way to get started with developing a module is using the [Module Skeleton](). It provides you with a pre configured project and a structure aswell as all dependencies needed:

- ready-to-use VS2019 MSVC++ project with easy customization
    - supports debugging of your project
- all dependencies needed for module development included
    - [Proxy]() and its dependencies are included 

Learn how to make your own module using [Module Skeleton](https://gitlab.com/blrevive/tools/module-skeleton).

